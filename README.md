# wemos


## References
* Driver: https://wiki.wemos.cc/downloads
* Guide for pycharm: https://blog.jetbrains.com/pycharm/2018/01/micropython-plugin-for-pycharm/
* Readme fo plugin for pycharm: https://github.com/vlasovskikh/intellij-micropython

## PyCharm Setup (OSX)
 * Make sure to install the serial driver for USB
 * Go to plug-ins and install micropython (make sure you select the marketplace tab if you cant find anything)
 * create a python environment which has python 3.5 (Don't use 3.6! See vlasovskikh guide)
 * under "language & framework" go to micropython and enable it (and select ESP8266) 
 * For the USB device location, refer to the USB serial driver documentation included with the download (might be: /dev/tty.wchusbserial1d110 )
 
## Flash single file
 * Under your run configurations, edit current configuration
 * Add Micropython and specify the file you want to flash
 
## Flash multiple files / project
 * Same as single file, but after creating, edit it and remove the filename and specify the root dir
 

## Gotcha's
 * To run REPL: Tools -> Micropython -> Micropython REPL
 * Deepsleep requires pin D0<>RST connected
 * When in deepsleep and the sleepcycle is long and the running state short (ie: you cant upload a new file anymore) unhook D0<>RST. When it wakes it won't sleep instantly but stay in a low power state in which you can flash
 * When flashing files make sure to use the root path and not files specifically. If done, you will only flash the current file (duh) and not other projectfiles
 * To run an interactive shell (on the wemos) run REPL and execute: `from upysh import *`
 * When using subfolders for multiple wemos devices use a custom shell command to flash the file:
 `/Users/markprins/.pyenv/versions/wemos/bin/python "/Users/markprins/Library/Application Support/PyCharm2018.3/intellij-micropython/scripts/microupload.py" -C /Users/markprins/dev/wemos/wemos_test -v /dev/tty.wchusbserial1d110 /Users/markprins/dev/wemos/wemos_test/main.py`
 * When you don't want nested projects. Don't use the wemos_test folder (or other title) and put files in the root. Now you can use the 'flash' option as is intended in the guides
 * It is usefull to use the shortcuts for running the configuration (ctrl-alt-r)
 * It is usefull to map shortcuts to launching REPL (ctrl-alt-w)
 * When you can't flash or upload hangs indefinately, consider following these steps:
    * close REPL window
    * reboot device using the reset button
    * Imediately start the REPL (ctrl-alt-w)
    * now you're in the REPL therminal
    * close it (cmd-w) and immediately run the flash command
    