import utime
from machine import Pin

import socket
import network
import machine
import urequests as requests
import json
import time


def blink(time_per_pulse, count):

    led = Pin(2, Pin.OUT)

    for i in range(0, count+1):
        led.off()
        utime.sleep_ms(int(time_per_pulse/2))
        led.on()
        utime.sleep_ms(int(time_per_pulse/2))


def deepsleep(time, sec=True):
    print("setting timer for " + str(time) + " sec")
    if sec:
        time = time * 1000

    print("setup alarm timer")
    rtc = machine.RTC()

    rtc.irq(trigger=rtc.ALARM0, wake=machine.DEEPSLEEP)

    rtc.alarm(rtc.ALARM0, time)

    print("Deep sleeping now")

    # blink(50, 1)
    machine.deepsleep()


def get_rssi():
    sta_if = network.WLAN(network.STA_IF)
    res = sta_if.scan()
    rssi = -1000

    for item in res:
        if item[0].decode('ASCII') == 'Prins':
            item_rssi = item[3]
            if item_rssi > rssi:
                rssi = item_rssi

    return rssi


def ifsetup():

    sta_if = network.WLAN(network.STA_IF)
    ap_if = network.WLAN(network.AP_IF)
    sta_if.active()
    ap_if.active()
    sta_if.connect('Prins', 'pr1n554p')

    while not sta_if.isconnected():
        blink(20, 1)
        # utime.sleep_ms(250)


    print("IFConfig", sta_if.ifconfig())


def main():

    start = time.ticks_ms()

    if machine.reset_cause() == machine.DEEPSLEEP_RESET:
        print('woke from a deep sleep')
    else:
        print('power on or hard reset')

    print("running main")

    ifsetup()

    mhz19Type = 2000                ## type of sensor (2000|5000)
    mhz19 = machine.Pin(4, Pin.IN)  ## GPIO4 == D2 on WemosD1Mini
    # hold_time_sec = 10
    # last_trigger = -10

    tHigh = 0
    tLow = 0
    ppm = 0
    prevVal = 0

    t1 = time.ticks_ms()
    high = t1
    low = t1


    results = []


    """
        MH-Z19B  (https://www.winsen-sensor.com/d/files/infrared-gas-sensor/mh-z19b-co2-ver1_0.pdf)
        PWM signal:
        Range: 0 ~ 2000 ppm (Default operation, dont know how you can select 5000ppm tho)
        Pulse cycle: 1004ms (+/- 5%)
        Start Cycle: 2ms (theoretical)
        Middle Cycle: 1000ms
        End Cycle (low) 2ms
        
        Approach:
        - if 1, wait for 0
        - when 0, wait for 1
        - start timer
        - when 0 again, end timer
        - determine time running (tEnd - tStart)
        - subtract 
        
        
        
        
    """

    run = True
    it = 0

    lowTime = 0
    highTime = 0

    tS = time.ticks_ms()
    while time.ticks_ms() - tS < 10040: # we sample 10 seconds of data
        if mhz19.value():
            highTime += 1
        else:
            lowTime += 1
        utime.sleep_us(850)




    #ppm = ((highTime / ( highTime + lowTime) * 10040) - 40) * 0.2

    highActual = (highTime / ( highTime + lowTime) * 1004) # comparison is done over 10 seconds, but convert/average it to the 1004ms in documentation
    lowActual =  (lowTime  / ( highTime + lowTime) * 1004)

    ppm = 5000 * (highActual - 2) / (highActual + lowActual - 4)

    #
    # pwmLast = -1            ## default denoting it is unknown
    # tS = time.ticks_ms()    ## Start Time (of an interval)
    # tB = tS                 ## Begin Time (of the measure cycle)
    # tMd = 10040             ## Measure Duration
    # times = [0, 0, 0]        ## result storage
    # measurements = 0        ## Succesful measurements taken
    #
    # while run:
    #     if mhz19.value() != pwmLast:
    #         tE = time.ticks_ms()
    #         if tE - tB > tMd:
    #             run = False
    #         else:                       ## store if we changed and not supposed to stop
    #             times[pwmLast] = tE - tS
    #             measurements += 1
    #             tS = tE
    #     else:
    #         utime.sleep_us(850)
    #
    # tL = times[0] / measurements
    # tH = times[1] / measurements
    # ppm = 5000 * (tL - 2 ) / (tH + tL - 4)
    # tE = time.ticks_ms()
    # tT = tE - tS
    # print("Time Taken: ", tT)
    # print("ppm: ", ppm)




    # total = highTime + lowTime
    # high_perc = highTime / total
    # low_perc = lowTime / total
    # realHigh = high_perc * 1004
    # print("total ", total, "high perc ", high_perc, "low_perc", low_perc, "high real: ", realHigh, "high real corrected", realHigh - 4, "ppm:", (realHigh - 4) * 2)

    tE = time.ticks_ms()
    tT = tE - tS
    print("Time Taken: ", tT)
    print("ppm: ", ppm)
    # print("Low Time: ", lowTime)
    # print("High Time: ", highTime)



    # for it in range(0, 10):
    #
    #     run = True
    #
    #     while run:
    #
    #         pwm_value = mhz19.value()   ## Value of PWM line (1|0)
    #         t1 = time.ticks_ms()        ## register current time in ms
    #
    #         if pwm_value == 1:
    #
    #             if pwm_value != prevVal:
    #
    #                 high = t1
    #                 tLow = high - low
    #                 prevVal = pwm_value
    #
    #         else:
    #             if pwm_value != prevVal:
    #                 low = t1
    #                 tHigh = low - high
    #                 prevVal = pwm_value
    #
    #                 if tLow != 0:
    #
    #                     # as per documentation:  Cppm = 2000|5000 * (tHigh - 2ms) / (tHigh + tLow - 4ms)
    #                     # where tHigh = time for high level during an output cycle and tLow is time for low level
    #                     ppm = mhz19Type * (tHigh - 2) / (tHigh + tLow - 4)
    #                     print("PPM = ", ppm)
    #                     if it != 0:
    #                         results.append(ppm)  ## skip first measure
    #                     run = False
    #
    #     # utime.sleep_ms(7000)
    #
    # avg = int(sum(results) / len(results))
    # print(results, avg)


    # base64string = base64.encodestring(('%s:%s' % (domoticzusername, domoticzpassword)).encode()).decode().replace('\n', '')
    base64string = 'Ogo=' # base64(':')


    url = 'http://192.168.1.92:8080/json.htm?type=command&param=udevice&idx=2026&nvalue=' + "%s" % (ppm)

    print("url", url)

    r = requests.request(method='get',
                         url=url,
                         headers={
                             "Authorization": base64string
                         })

    print(r, r.content, r.text)

    end = time.ticks_ms()

    exec_time = end - start

    print(start, end, exec_time)

    sleep_time = 30000# milliseconds

    sleep_time = sleep_time - exec_time

    if sleep_time < 0:
        # if we have waited to long, just wait some, not much
        sleep_time = 5000

    deepsleep(sleep_time, sec=False)


if __name__ == '__main__':

    # We just wrap a big try/catch because it seems sometimes there is a snag. I don't want to check the unit and
    # restart it as I wont see anything. It could probably be done a better way but for now it will work.
    try:
        main()
    except Exception as e:
        # when everything fails, just go to sleep and wait for the next round.

        # log.critical('--- Caught Exception ---')
        # import sys
        # sys.print_exception(e)
        # log.critical('----------------------------')
        blink(500, 5)
        deepsleep(2000, sec=False)






