import utime
from machine import Pin

import socket
import network
import machine
import urequests as requests
import json
import time
import onewire, ds18x20
from ubinascii import hexlify

def blink(time_per_pulse, count):

    led = Pin(2, Pin.OUT)

    for i in range(0, count+1):
        led.off()
        utime.sleep_ms(int(time_per_pulse/2))
        led.on()
        utime.sleep_ms(int(time_per_pulse/2))


def deepsleep(time, sec=True):
    print("setting timer for " + str(time) + " sec")
    if sec:
        time = time * 1000

    print("setup alarm timer")
    rtc = machine.RTC()

    rtc.irq(trigger=rtc.ALARM0, wake=machine.DEEPSLEEP)

    rtc.alarm(rtc.ALARM0, time)

    print("Deep sleeping now")

    # blink(50, 1)
    machine.deepsleep()


def get_rssi():
    sta_if = network.WLAN(network.STA_IF)
    res = sta_if.scan()
    rssi = -1000

    for item in res:
        if item[0].decode('ASCII') == 'Prins':
            item_rssi = item[3]
            if item_rssi > rssi:
                rssi = item_rssi

    return rssi


def ifsetup():

    sta_if = network.WLAN(network.STA_IF)
    ap_if = network.WLAN(network.AP_IF)
    sta_if.active()
    ap_if.active()
    sta_if.connect('Prins', 'pr1n554p')

    while not sta_if.isconnected():
        blink(20, 1)
        # utime.sleep_ms(250)


    print("IFConfig", sta_if.ifconfig())


def main():

    start = time.ticks_ms()

    if machine.reset_cause() == machine.DEEPSLEEP_RESET:
        print('woke from a deep sleep')
    else:
        print('power on or hard reset')

    print("running main")

    ifsetup()

    data = {
        "rssi": "%s" % (get_rssi())
    }

    js = json.dumps(data)

    print(js)

    #base64string = base64.encodestring(('%s:%s' % (domoticzusername, domoticzpassword)).encode()).decode().replace('\n', '')
    base64string = 'Ogo=' # base64(':')


    url = 'http://192.168.1.92:8080/json.htm?type=command&param=udevice&idx=1974&nvalue=0&svalue=' + "%s" % (get_rssi())

    print("url", url)

    r = requests.request(method='get',
                         url=url,
                         headers={
                             "Authorization": base64string
                         })

    print(r, r.content, r.text)


    end = time.ticks_ms()

    exec_time = end - start

    print(start, end, exec_time)

    sleep_time = 30000# milliseconds

    sleep_time = sleep_time - exec_time

    deepsleep(sleep_time, sec=False)


if __name__ == '__main__':
    main()


