import utime
from machine import Pin

import socket
import network
import machine
import urequests as requests
import json
import time
import onewire, ds18x20
from ubinascii import hexlify

def blink(time_per_pulse, count):

    led = Pin(2, Pin.OUT)

    for i in range(0, count+1):
        led.off()
        utime.sleep_ms(int(time_per_pulse/2))
        led.on()
        utime.sleep_ms(int(time_per_pulse/2))


def deepsleep(time, sec=True):
    print("setting timer for " + str(time) + " sec")
    if sec:
        time = time * 1000

    print("setup alarm timer")
    rtc = machine.RTC()

    rtc.irq(trigger=rtc.ALARM0, wake=machine.DEEPSLEEP)

    rtc.alarm(rtc.ALARM0, time)

    print("Deep sleeping now")

    # blink(50, 1)
    machine.deepsleep()


def get_rssi():
    sta_if = network.WLAN(network.STA_IF)
    res = sta_if.scan()
    rssi = -1000

    for item in res:
        if item[0].decode('ASCII') == 'Prins':
            item_rssi = item[3]
            if item_rssi > rssi:
                rssi = item_rssi

    return rssi


def ifsetup():

    sta_if = network.WLAN(network.STA_IF)
    ap_if = network.WLAN(network.AP_IF)
    sta_if.active()
    ap_if.active()
    sta_if.connect('Prins', 'pr1n554p')

    while not sta_if.isconnected():
        blink(20, 1)
        # utime.sleep_ms(250)


    print("IFConfig", sta_if.ifconfig())


def main():

    start = time.ticks_ms()

    if machine.reset_cause() == machine.DEEPSLEEP_RESET:
        print('woke from a deep sleep')
    else:
        print('power on or hard reset')

    print("running main")

    ifsetup()

    data = {
        "rssi": "%s" % (get_rssi())
    }

    js = json.dumps(data)

    print(js)

    #base64string = base64.encodestring(('%s:%s' % (domoticzusername, domoticzpassword)).encode()).decode().replace('\n', '')
    base64string = 'Ogo=' # base64(':')

    hex_dict = {
        b'28ff8699b1170499': 2003,  # vvw1
        b'28aad54c1a1302d6': 2004,  # vvw2
        b'28aafe4b1a1302d8': 2005,  # vvw3
        b'28aa1edb19130207': 2006,  # vvw4
        b'28aa1ecd19130236': 2007,  # vvw5
        b'28aa064f1a13024e': 2008,  # vvw6
        b'28aaca531a130267': 2009,  # vvw7
        b'28aaf2521a130252': 2010,  # vvw8
        b'28aad4551a1302b0': 2011,  # vvw9
        b'28aac0461a130263': 2012   # vvw10
    }

    # the device is on GPIO12
    dat = machine.Pin(12)

    # create the onewire object
    ds = ds18x20.DS18X20(onewire.OneWire(dat))

    # scan for devices on the bus
    roms = ds.scan()

    if len(roms) > 0:

        print('found devices:', roms)
        ds.convert_temp()
        time.sleep_ms(750)
        it = 0
        for rom in roms:
            it += 1
            idb = hexlify(rom)
            idx = hex_dict[idb]

            temp = ds.read_temp(rom)
            print(it, idb, idx, temp)

            url = 'http://192.168.1.92:8080/json.htm?type=command&param=udevice&idx=%s&nvalue=0&svalue=%s' % (
                idx,
                temp)

            print("url", url)

            r = requests.request(method='get',
                                 url=url,
                                 headers={
                                     "Authorization": base64string
                                 })
            print(r, r.content, r.text)
    else:
        print("No devices found, ")

    end = time.ticks_ms()

    exec_time = end - start

    print(start, end, exec_time)

    sleep_time = 30000# milliseconds

    sleep_time = sleep_time - exec_time

    deepsleep(sleep_time, sec=False)


if __name__ == '__main__':

    # We just wrap a big try/catch because it seems sometimes there is a snag. I don't want to check the unit and
    # restart it as I wont see anything. It could probably be done a better way but for now it will work.
    try:
        main()
    except Exception as e:
        # when everything fails, just go to sleep and wait for the next round.
        blink(500, 5)
        deepsleep(2000, sec=False)
